set -eux

. ./lib.sh

clean
sleep 3
init
partition
config_mkinitcpio
bootstrap
configure
packages
networking
bootloader
paru
#unmount
