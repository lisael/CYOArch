# CYOArch

Create your own archlinux.

Well actchualy it's more MY own arch, BTW. That said it has sensible defaults
for a work-ready laptop, so I just dump this here to share. You most probably
want to fork this repo and tweak the main script to fit your needs.

## Scope

The goal is the basic install of a working linux from the bootloader to pacman
tweaks. This is not a distribution of any kind, only the packages requierd by
the installation process are installed. Maybe I'll add a way to install a list
of packages provided as a side txt file, but I won't maintain this if it's more
than 10 sh LoC.

## Features and opiniated choices

- LVM over Luks
- Grub 
- unencrypted /boot. Everything else is.
- BTRFS
- systemd-networkd
- paru AUR helper
- encrypted hibernate/resume

## Comming soon

- Snapper with pac-snap

## Goals

- Install my laptop in less than 5 minutes. A seasoned Arch user that has already
  installed their machines a couple of time should spend 5-10 minutes reading the
  script and be able to run the installer comfidently.
- Being a nice example that (Arch)Linux newbies can read and study.
- less than 500 LoC of dumb simple sh. I'm no sh guru, everything should be clear
  and straightforward

## Usage

1. Fork this project
1. read, tweak, break, rebuild
2. Boot with the vanilla install archiso
3. clone your fork of the repo
   ```sh
    pacman -Sy git
    git clone https://gitlab.com/lisael/CYOArch.git
    cd CYOArch
    ```
4. run, `sh run.sh`
5. enjoy.

You may want to test on a virtual machine before, I for myself use a VirtualBox
that boots on the arch installer with small throwaway disk.

## License

!THIS IS NOT FREE SOFTWARE!

Well mostly it is, in the sense that you can use, copy, modify and distribute under
the same conditions. This section MUST be copied verbatim in all derivative works.

The only condition is that if you use this for your very first Arch installation,
you MUST read and understand all the Wiki sections cited in the code comments.
Failing to do so may lead to a broken system beyond your ability to repair. In
the eventuality of such an event, you are authorized to post your question on 
internet forums only after you posted a video of yourself dancing GAGNAM Style.
People with disabilities that may prevent them from performing the dance may
sing the full length of the "song".
