#! /bin/sh

export ARCHROOT="/mnt"

export LINUX_EXTRA_PARAMS=""

init() {
    # check that we did boot with efi
    ls /sys/firmware/efi/efivars
    # check the connection
    ping -c 1 archlinux.org
    # set the clock
    timedatectl set-ntp true
    pacman --noconfirm -S fzf
}

unmount_chroot(){
    umount ${ARCHROOT}/boot || true
    umount ${ARCHROOT} || true
}

archroot(){
    eval "arch-chroot \"${ARCHROOT}\" $@"
}

# create the btrfs root partition, snapper ready
# usage: setup_btrfs /dev/a/partion ${ARCHROOT}/a/mountpoint
setup_btrfs() {
    part=$1
    mnt=$2
    umount "${part}" || true
    mkfs.btrfs -f "${part}"
    mount "${part}" "${mnt}"
    btrfs subvol create "${mnt}/@"
    btrfs subvol set-default "${mnt}/@"
    umount "${mnt}"
    mount "${part}" "${mnt}"
    mkdir -p "${mnt}/usr"
    btrfs subvol create "${mnt}/home"
    btrfs subvol create "${mnt}/var"
    btrfs subvol create "${mnt}/opt"
    btrfs subvol create "${mnt}/srv"
    btrfs subvol create "${mnt}/usr/local"
    umount "${mnt}"
}

# disable the swap partition, given a drive
# usage: `disable_swap /dev/sda` -> swapoff /dev/sdax 
disable_swap() {
    drive=$1
    swapon | grep "$drive" && swapoff $(swapon | grep -oe "${drive}\w\?") || true
}


ask_install_drive(){
    set +x
    echo "########################################################" >&2
    echo "# Select the installation drive" >&2
    echo "########################################################" >&2
    echo "Press enter..." >&2
    read
    set -x

    ls -1 /dev | egrep "sd.$|nvme.n.$|mmc.*|md.*" | fzf
}


cleanse_lvm(){
    vg=$1
    for part in $(ls "/dev/${vg}/"); do
        disable_swap $(readlink -f "/dev/${vg}/${part}")
    done
    lvremove -yff "${vg}" || true
    vgremove -yff "${vg}" || true
}

cleanse_cryptpart(){
    cryptpart=$1
    pvremove -yff "/dev/mapper/${cryptpart}" || true
    cryptsetup close "/dev/mapper/${cryptpart}" || true
}

clean(){
    cleanse_lvm vgroot
    cleanse_cryptpart cryptlvm
}


lvm_create(){
    drive=$1
    SWAPSPACE=$2
    pvcreate -yff "${drive}"
    vgcreate "vgroot" "${drive}"
    lvcreate -y -L ${SWAPSPACE} vgroot -n lvswap
    lvcreate -y -l 100%FREE vgroot -n lvroot
}


encrypt_part(){
    drive=$1
    cryptpart=$2
    cryptsetup luksFormat "${drive}"
    cryptsetup open "${drive}" "${cryptpart}"
}


partition(){
    DRIVE=$(ask_install_drive)

    if (<<<"$DRIVE" egrep "nvme.*|mmc.*|md.*" &> /dev/null) then
        PART_PREFIX="p"
    else
        PART_PREFIX=""
    fi

    disable_swap "/dev/${DRIVE}${PART_PREFIX}"
    unmount_chroot

    sgdisk --zap-all /dev/"$DRIVE"
    wipefs -a -f /dev/"$DRIVE"

    echo -e "n\n\n\n1024M\nef00\nn\n\n\n\n\nw\ny" | gdisk /dev/"${DRIVE}"

    BOOT="/dev/${DRIVE}${PART_PREFIX}1"
    CRYPT="/dev/${DRIVE}${PART_PREFIX}2"
    
    encrypt_part "${CRYPT}" "cryptlvm"
    CRYPT_UUID=$(lsblk -dno UUID ${CRYPT})
    export LINUX_EXTRA_PARAMS="${LINUX_EXTRA_PARAMS} cryptdevice=UUID=${CRYPT_UUID}:cryptlvm"

    LVM="/dev/mapper/cryptlvm"

    set +x
    printf "Swap size(M)? "
    read SWAPSPACE
    set -x
    
    lvm_create "${LVM}" "${SWAPSPACE}M"

    SWAP=/dev/vgroot/lvswap
    export LINUX_EXTRA_PARAMS="${LINUX_EXTRA_PARAMS} resume=${SWAP}"

    (
      wipefs -a "${SWAP}"
      mkswap "${SWAP}"
      swapon "${SWAP}"
    )

    ROOT="/dev/vgroot/lvroot"

    setup_btrfs "${ROOT}" ${ARCHROOT}

    (
        umount "${BOOT}" || true
        sgdisk --zap-all "$BOOT"
        wipefs -a "$BOOT"
        mkfs.vfat -F32 "$BOOT"
    )

    mount "${ROOT}" ${ARCHROOT}
    mkdir -p ${ARCHROOT}/boot
    mount "${BOOT}" ${ARCHROOT}/boot
}

config_mkinitcpio(){
    mkdir -p ${ARCHROOT}/etc/
    cat << EOF > ${ARCHROOT}/etc/mkinitcpio.conf
MODULES=()
BINARIES=()
FILES=()
HOOKS=(base udev autodetect keyboard keymap modconf block encrypt lvm2 resume filesystems fsck)
#COMPRESSION="lz4"
COMPRESSION_OPTIONS=()
EOF
}

bootstrap(){
    # reflector --country France --country Germany --age 12 --sort rate --save /etc/pacman.d/mirrorlist
    pacstrap ${ARCHROOT} base
    pacstrap -U ${ARCHROOT} pkgs/pacman*
    pacstrap -U ${ARCHROOT} pkgs/paru*
    cp /etc/pacman.d/mirrorlist ${ARCHROOT}/etc/pacman.d/mirrorlist 
    archroot pacman --noconfirm -Sy lvm2 linux linux-firmware vim btrfs-progs grub-btrfs
}

ask_timezone(){
    set +x
    echo "########################################################" >&2
    echo "Select timezone" >&2
    echo "########################################################" >&2
    echo "Press enter..." >&2
    read
    set -x

    zones=${ARCHROOT}/usr/share/zoneinfo
    major_zone=$(ls $zones | egrep '^[A-Z]' | fzf)
    zone=${zones}/${major_zone}
    if [ -d ${zone} ]; then
        minor_zone=$(ls $zone | egrep '^[A-Z]' | fzf)
        zone=${zone}/${minor_zone}
    fi
    echo ${zone} | sed 's|^/[^/]*||'
}

ask_locale(){
    set +x
    echo "########################################################" >&2
    echo "Select locale and charmap" >&2
    echo "########################################################" >&2
    echo "Press enter..." >&2
    read
    set -x
    locale=$(ls ${ARCHROOT}/usr/share/i18n/locales | fzf)
    charmap=$(ls -1 /usr/share/i18n/charmaps | sed 's/^\(.\+\)\.gz$/\1/' | fzf)

    echo "${locale}.${charmap} ${charmap}"
}


ask_hostname(){
    set +x
    hn=""
    while [ -z $( echo "$hn" | egrep "^[A-Za-z0-9-]+$" || true ) ]; do
        echo "########################################################" >&2
        echo "Hosname" >&2
        echo "########################################################" >&2
        echo 'Hostname? ( ^[A-Za-z0-9-]+$ )' >&2
        read hn
    done
    set -x
    echo ${hn}
}


configure(){
    genfstab -U ${ARCHROOT} > ${ARCHROOT}/etc/fstab
    # add the compress option to btrfs root (best for ssd)
    sed -i 's|\(\s\+btrfs\s\+\)|\1compress,ssd,|' /etc/fstab
    cat ${ARCHROOT}/etc/fstab

    HOSTNAME=$(ask_hostname)
    echo "${HOSTNAME}" > ${ARCHROOT}/etc/hostname
    cat << EOF > ${ARCHROOT}/etc/hosts
127.0.0.1    localhost
::           localhost
127.0.1.1    ${HOSTNAME}.localdomain ${HOSTNAME}
EOF

    timezone=$(ask_timezone)
    arch-chroot ${ARCHROOT} ln -sf /usr/share/zoneinfo/Europe/Paris /etc/localtime
    arch-chroot ${ARCHROOT} hwclock --systohc

    loc=$(ask_locale)
    echo ${loc} > ${ARCHROOT}/etc/locale.gen
    arch-chroot ${ARCHROOT} locale-gen

    lang=$(echo $loc | cut -d" " -f1)
    echo "LANG=$lang" > ${ARCHROOT}/etc/locale.conf

    arch-chroot ${ARCHROOT} passwd
}

packages(){
    arch-chroot ${ARCHROOT} pacman -Syu
    arch-chroot ${ARCHROOT} pacman --noconfirm -S \
        intel-ucode sudo grub man man-db man-pages texinfo \
        efibootmgr systemd-resolvconf base-devel git sudo
}

networking(){
    arch-chroot ${ARCHROOT} systemctl enable systemd-networkd.service
    arch-chroot ${ARCHROOT} systemctl enable systemd-resolved.service
    ln -sf ${ARCHROOT}/run/systemd/resolve/stub-resolv.conf ${ARCHROOT}/etc/resolv.conf
    cat << EOF > ${ARCHROOT}/etc/systemd/network/20-wired.network
[Match]
Name=enp*

[Network]
DHCP=yes
EOF
}


bootloader(){
    sed -i "s|^GRUB_CMDLINE_LINUX=.*$|GRUB_CMDLINE_LINUX=\"${LINUX_EXTRA_PARAMS}\"|" ${ARCHROOT}/etc/default/grub
    arch-chroot ${ARCHROOT} grub-install --target=x86_64-efi \
                                  --efi-directory=/boot \
                                  --bootloader-id=GRUB
    arch-chroot ${ARCHROOT} grub-mkconfig -o /boot/grub/grub.cfg
}

paru(){
    pacstrap -U ${ARCHROOT} pkgs/paru*
}

builder_user(){
    archroot useradd builder -m -G wheel
    sed -i 's/# \(%wheel.*NOPASSWD\)/\1/' ${ARCHROOT}/etc/sudoers
}

user_packages(){
    # cp ./pkgs.txt ${ARCHROOT}/home/builder
    cp ./pkgs.txt ${ARCHROOT}/root
    arch-chroot /mnt sh -c 'pacman -S --needed - < /root/pkgs.txt'
}
